'use client'

import Image from 'next/image'
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { useState } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dropdown } from 'primereact/dropdown';
import { Sidebar } from 'primereact/sidebar';

export default function Home() {

  const c = console

  const [campo1, setCampo1] = useState()
  const [campo2, setCampo2] = useState()
  const [documento, setDocumento] = useState('')
  const [tercero, setTercero] = useState([{
    terctido: 'Cedula',
    tercdocu: '6134461',
    tercdesc: 'Leonardo paz'
  }])
  const [visible, setVisible] = useState(false)

  const listTipoDocu = [
    {
      codigo: 'cc',
      descripcion: 'Cedula'
    },
    {
      codigo: 'ti',
      descripcion: 'Tarjeta identidad'
    }
  ]

  const sendForm = (e) => {

    e.preventDefault()

    const objDocu = listTipoDocu.find(docu => docu.codigo === documento.codigo)

    const data = {
      terctido: objDocu?.descripcion,
      tercdocu: campo1,
      tercdesc: campo2
    }


    // c.log('data', [...tercero], [data])
    //c.log('documento', documento)

    setTercero((elemnts) => [...elemnts, data])

  }


  return (
    <main>
      <div>

        <div className='flex justify-content-center'>
          <h1 className='title'>Hola manual portal primereact</h1>
        </div>

        <form>

          <h2>Sistema de girlla primeflex</h2>
          <div className="grid flex justify-content-center block">
            <div className="col">
              <div className="text-center p-3 border-round-sm bg-primary font-bold">Grilla campo 1</div>
            </div>
            <div className="col">
              <div className="text-center p-3 border-round-sm bg-primary font-bold ">Grilla campo 2</div>
            </div>
            <div className="col">
              <div className="text-center p-3 border-round-sm bg-primary font-bold ">Grilla campo 1</div>
            </div>
          </div>


          <div className='block'>
            <div className='grid flex justify-content-center'>
              <div className="col-12">
                <div>
                  <label htmlFor="tex-docu" >Tipo Documento</label>
                </div>

                <Dropdown
                  value={documento}
                  onChange={(e) => setDocumento(e.value)}
                  options={listTipoDocu}
                  optionLabel="descripcion"
                  placeholder="--Seleccion un documento--"
                  className="w-full" />

              </div>
              <div className="col-6">
                <div>
                  <label htmlFor="tex-docu" >Documento</label>
                </div>
                <InputText id='tex-docu' type="text" name="campo1" className='w-full' onChange={(e) => setCampo1(e.target.value)} />
              </div>
              <div className="col-6">
                <div>
                  <label htmlFor="tex-nomb">Nombre</label>
                </div>
                <InputText id='tex-nomb' type="text" name="campo2" className='w-full' onChange={(e) => setCampo2(e.target.value)} />
              </div>
              <div className="col-12">
                <Button label="Enviar" className='w-full' onClick={sendForm} />
              </div>
            </div>
          </div>


          <div className='block'>
            <h2>Data tables</h2>
            <DataTable value={tercero} >
              <Column field="terctido" header="Tipo Documento"></Column>
              <Column field="tercdocu" header="Documento"></Column>
              <Column field="tercdesc" header="Nombre"></Column>
            </DataTable>
          </div>


        </form>

        <div className="card flex justify-content-center">
          <Sidebar
            visible={visible}
            onHide={() => setVisible(false)}>
            <h2>Sidebar</h2>
            <p> Hola  manual portal </p>
          </Sidebar>
          <Button label='SideBar' icon="pi pi-arrow-right" onClick={() => setVisible(true)} />
        </div>
      </div>

      
    </main>
  )
}
